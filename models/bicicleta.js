var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: "2dsphere", sparse: true },
    },
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({code: code, color: color, modelo: modelo, ubicacion: ubicacion});
}
bicicletaSchema.statics.allBicis = function (cb) {return this.find({}, cb);}

bicicletaSchema.statics.add = function (bici, cb) {
    this.create(bici, cb);
}

bicicletaSchema.method.toString = function () {return `ID: ${this.code} | Color: ${this.color}`};


module.exports = mongoose.model('Bicicleta', bicicletaSchema);