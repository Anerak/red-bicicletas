var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = (req, res) => {
    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}


exports.bicicleta_update = (req, res) => {
    let newData = req.body;
    let bici = Bicicleta.findById(newData.id);
    bici.id = newData.id;
    bici.color = newData.color
    bici.modelo = newData.modelo
    bici.ubicacion = [newData.lat, newData.lng];
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}