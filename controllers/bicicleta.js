let Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = (req, res) => {
    let dataBody = req.body;
    let bici = new Bicicleta(dataBody.id, dataBody.color, dataBody.modelo);
    bici.ubicacion = [dataBody.lat, dataBody.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = (req, res) => {
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = (req, res) => {
    let bici = Bicicleta.findById(req.params.id)
    let dataBody = req.body;
    bici.id = dataBody.id;
    bici.color = dataBody.color;
    bici.modelo = dataBody.modelo;
    bici.ubicacion = [dataBody.lat, dataBody.lng];

    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}