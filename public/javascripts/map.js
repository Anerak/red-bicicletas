/* // Mapa en tu posición real
function getCoords (position) {
    var coords = [position.coords.latitude, position.coords.longitude];
    var mymap = L.map('main_map').setView(coords, 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        tileSize: 512,
        zoomOffset: -1,
    }).addTo(mymap);
    L.marker(coords).addTo(mymap)
    .bindPopup(coords.toString())
    .openPopup();
}

window.addEventListener('load', navigator.geolocation.getCurrentPosition(getCoords, (err) => console.log(err),{maximumAge: 10000, timeout: 5000,enableHighAccuracy: true}));

*/

let mymap = L.map('main_map').setView([-34.6012424, -58.3861497], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1.
}).addTo(mymap);

fetch('api/bicicletas', {
    headers: {
        'Content-Type': 'application/json'
    },
}).then((res) => res.json())
.then((data) => data.bicicletas.forEach((i) => {
    L.marker(i.ubicacion)
    .addTo(mymap)
    .bindPopup(`#${i.id} ${i.color} ${i.modelo}`)
    .openPopup();
}));