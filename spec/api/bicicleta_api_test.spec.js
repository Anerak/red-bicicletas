/*var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', () => {
    describe('GET Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var newBike = new Bicicleta(1, 'black', 'urban', [-34.015615, 101.865488]);
            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
            });
        });
    });
    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'Content-Type': 'application/json'};
            var newBike = `{"id": 10, "color": "red", "modelo": "urban", "lat": -34, "lng": -54}`;
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: newBike,
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('red');
                done();     // Jasmine expects done() to finish the execution. Useful for async operations.
            });
        });
    });

    describe('POST Bicicletas /update', () => {
        it('Status 200', (done) => {
            var newBike = `{"id": 20, "color": "red", "modelo": "urban", "lat": -34, "lng": -54}`;
            Bicicleta.add(newBike);
            newBike = `{"id": 20, "color": "fuchsia", "modelo": "beach", "lat": -37, "lng": -53}`;
            request.post({
                header: {'Content-Type': 'application/json'},
                url: 'http://localhost:3000/api/bicicletas/update',
                body: newBike
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe('POST Bicicletas /delete', () => {
        it('Status 204', (done) => {
            var newBike = new Bicicleta(1, 'black', 'urban', [-34.015615, 101.865488]);
            Bicicleta.add(newBike);
            request.post({
                headers: {"Content-Type": "application/json"},          // I'm making a note here.
                url: 'http://localhost:3000/api/bicicletas/delete',     // Without the headers and the fancy quotes on body,
                body: `{"id": 1}`                                       // it won't work.
            }, (error, response, body) => {
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });
});*/