var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {
    beforeEach(() => {
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', () => {
            console.log('We are connected');
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Creates a new instance of Bicicleta', () => {
            let bici = Bicicleta.createInstance(1, "green", "urban", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("green");
            expect(bici.modelo).toBe("urban");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Start empty',(done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });


    describe('Bicicleta.add', () => {
        it('Add Bicicleta', (done) => {
            let newBike = new Bicicleta({code: 1, color: "fuchsia", modelo: "beach"});

            Bicicleta.add(newBike, (err, bicis) => {
                if (err) console.log(err);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(newBike.code);
                    done();
                });
            });
        });
    });
});